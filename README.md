# PoetryGeneration_William_Syl-worth

Syllable Neural Language Model for English Poem Generation

Repo contents:
configs
datasets
experiments
models
utils

Use the python script wordsworth_sy_exp.py to run an experiment using the syllable language model

The contents of these folders are as follows:

configs → sy_lm_configs.py
	 
datasets →  word_syllable_dataset.py	
		 →  syllable_Dataset.py
		 
experiments →  experiment.py	
			→  syllable_lm_experiment.py
			
models →  basics.py
	   →  language_model.py
	   
utils →  utils.py
	  →  vocabulary.py
	  
	  
===============================================================================================
TO RUN AN EXPERIMENT 

Dependancies:
Tensorflow 1.4
NLTK.tokenize
NLTK.corpus
hyphenate
matplotlib.pyplot
pandas
numpy
pickle
re
sklearn
math
collections
random
seaborn
wordcloud


→ Use wordsworth_sy_exp.py 

This script performs a full training and validation experiment on your chosen data. 
- It will print the training loss and perplexity score for each Epoch number, 
- Print the ground truth and prediction for validation 
- Validation loss and perplexity score
- it will stop training after the number of stated epochs are up, or unless the validation loss doesnt improve after K epochs,
- best model weights are saved at the point of the best loss and best perplexity
- all model weights, configs, logs and generated outputs are saved to the folder stated
- performs validation on the held out test set and prints test perplexity

Change the flag from 'train' to 'eval' and this will generate text from a trained model.
- prints the IDs and words of each generation 
