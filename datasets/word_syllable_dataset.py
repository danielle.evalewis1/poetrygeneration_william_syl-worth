# This script creates the wordsworthSyLMDatasetProduction and wordsworthSyLMDatasetGuide classes

# The classes load the data from source, use the tokenizing and hyphenating functions 
# and add EOV token to the end of each verse then shuffle the verses

#import SyLMDataset class from datasets folder 
from datasets.syllable_dataset import SyLMDataset

#import functions from utils 
from utils.utils import  get_1_verse_production, get_hyp_lm_verses_works, get_hyp_lm_verses_guide, get_guide_to_the_lakes


# Mentioned in 3.2.2 Building the dataset
# class used for Production and Prelude

class wordsworthSyLMDatasetProduction(SyLMDataset):
    def __init__(self, config, sy_vocab=None):
        """
        Class to create a dataset from wordsworth's prelude / Production.
        :param config: a Config object
        :param sy_vocab: (optional) a Vocabulary object where tokens of the dictionary
        are syllables. If None, the vocabulary is create automatically from the source.
        """
        super().__init__(config, sy_vocab)

    def load(self, sources):
        """
        Load examples from dataset and prepares the data.
        Cleans, tokenizes and hyphenates.
        :param sources: data filepath.
        :return:
        """
        # USE THIS FOR THE PRELUDE  & PRODUCTION
        #encodings needs to be utf-8 for Prelude, None for Production
        # Tokenizes
        verses, words = get_1_verse_production(filename=sources, encoding = 'UTF-8') #encoding =None

        # Hyphenates verses
        verses1 = get_hyp_lm_verses_works(verses)

        x = []
        for v in verses1:
            x.append([])
            for verse in v:
                x[-1].extend(verse) 
                x[-1].append("<EOV>") #adds EOV token to the end of each verse

        x = self.shuffle(x) #shuffles the hyphenated verses 
        return x
    
#class used for Guide    
    
class wordsworthSyLMDatasetGuide(SyLMDataset):
    def __init__(self, config, sy_vocab=None):
        """
        Class to create a dataset from Wordsworth's Guide to the lakes.
        :param config: a Config object
        :param sy_vocab: (optional) a Vocabulary object where tokens of the dictionary
        are syllables. If None, the vocabulary is create automatically from the source.
        """
        super().__init__(config, sy_vocab)

    def load(self, sources):
        """
        Load examples from dataset and prepares the data.
        Cleans, tokenizes and hyphenates.
        :param sources: data filepath.
        :return:
        """

        # encodings needs to be utf-8 for the Guide
        # Tokenizes 
        verses, words = get_guide_to_the_lakes(filename=sources, encoding='utf-8')

        # Hyphenates
        verses1 = get_hyp_lm_verses_guide(verses)

        x = []
        for v in verses1:
            x.append([])
        
            x[-1].extend(v)
            x[-1].append("<EOV>") #adds EOV token to the end of each verse

        x = self.shuffle(x) #shuffles the hyphenated verses
        return x