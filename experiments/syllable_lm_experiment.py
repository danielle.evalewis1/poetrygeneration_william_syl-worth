
# This script creates the main experiment class used to run experiments.

# To run a training session, 'run' is used from the SyLMExperiment class. 
# This function uses _run_train_session which trains the model and outputs predictions 
# against the ground truth using run_train_op (from LanguageModel class). 
# The loss and perplexities are computed and then after each epoch the model is
#  validated on the validation set using _validation_op. 
# The model runs to the number of epochs, unless early stopping is enforced due to poor performance. 
# To test the model on the held-out test-set, use 'test' function from the SyLMExperiment class 


# Import libraries
import os
import tensorflow as tf
import numpy as np

# import classes and functions from submission code 
from experiments.experiment import Experiment
from utils.utils import print_and_write, print_paired_output


#define val_size and gen_size
tf.app.flags.DEFINE_integer('val_size', 400, 'size of the validation') #used for production and prelude
#tf.app.flags.DEFINE_integer('val_size', 45, 'size of the validation') #used for Guide 
tf.app.flags.DEFINE_integer('gen_size', 20, 'size of the generation') #used for all

# Experiment CLass

class SyLMExperiment(Experiment):
    def __init__(self, model, model_val, model_gen, dataset, exp_path, config):
        super().__init__(model, model_val, model_gen, dataset, exp_path, config)

    @staticmethod
    def get_loss(output):
        """Picks the proper position from an iterable of elements"""
        return output

    @staticmethod
    def get_preds(output):
        """Picks the proper position from an iterable of elements"""
        return output

# Writes the training logs 
    def _updt_train_logs(self, logs, batch_outputs, step):
        super()._updt_train_logs(logs, batch_outputs, step)

        print_and_write(logs, "\nLoss: " + str(batch_outputs["loss"]))
        print_and_write(logs, " PPL: " + str(batch_outputs["ppl"]))

# Writes the validation logs        
    def _updt_val_logs(self, logs, val_outputs, step):
        super()._updt_val_logs(logs, val_outputs, step)
   
        print_and_write(logs, "\nValidation Loss: " + str(np.mean(np.array([o["loss"] for o in val_outputs]))))
        print_and_write(logs, " Validation PPL: " + str(np.mean(np.array([o["ppl"] for o in val_outputs]))) + "\n")

# Prints Ground Truth and Prediction 
    def render(self, logs, examples, preds, gen_size):
        print_paired_output(logs, self.dataset.val_y[:gen_size], preds[:gen_size], self.dataset.vocabulary.rev_dictionary,
                            special_tokens=[self.dataset.vocabulary.word2id("<PAD>"), 0, self.dataset.vocabulary.word2id("<SEP>"),
                                            self.dataset.vocabulary.word2id("<GO>"), self.dataset.vocabulary.word2id("<EOS>")],
                            end_of_tokens=[self.dataset.vocabulary.word2id("<EOV>")])


    def _get_val_batch(self, val_range):
        start, end = val_range
        return self.dataset.val_x[start:end], self.dataset.val_y[start:end]

    def _get_gen_batch(self, gen_size):
        return self.dataset.val_x[:gen_size], self.dataset.val_y[:gen_size]


    @staticmethod
    def _get_test_batch(dataset, test_size):
        start, end = test_size
        return dataset.test_x[start:end], dataset.test_y[start:end]


    def run(self):
        """
        Runs a training experiment and logs the performance.
        
        return: best loss , best step 
        """
        logs = os.path.join(self.exp_path, "train_logs.txt")
        logs = open(logs, "w")

        val_summaries = []
        # Summaries
        train_summary = self.add_visualization_summaries("Train PPL", self.model.ppl)
        val_summary = self.add_visualization_summaries("Valid PPL", self.model_val.ppl)

        val_summaries.append(val_summary)

        best_loss, best_step = self._run_train_session(logs, train_summaries=[train_summary], val_summaries=val_summaries,
                                                       val_size=FLAGS.val_size, gen_size=FLAGS.gen_size)

        return best_loss, best_step

   
    