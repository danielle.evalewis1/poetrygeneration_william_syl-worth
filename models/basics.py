# This script creates the cell function and SimpleNlpRnn class used in Languagemodel class
# The LSTM cell was created using tf.contrib.rnn.BasicLSTMCell. 
# The SimpleNlpRnn class builds the RNN cell using the LSTM cell and returns the state and outputs. 
# The LanguageModel class (in script language_model.py) builds the language model using the SimpleNlpRnn cell. 



#Import TF1
import tensorflow as tf


def cell(size, cell_type, dropout=None, layers=1, config=None):
    cells = []
    cell = None
    for _ in range(layers): 
        if cell_type == "LSTM":
            cell = tf.contrib.rnn.BasicLSTMCell(size) # For LSTM RNN cell
        elif cell_type == "GRU":
            cell = tf.contrib.rnn.GRUCell(size) # For GRU RNN cell (not used)

        else:
            cell = tf.contrib.rnn.BasicRNNCell(size) #else most basic RNN cell

        if dropout:
            cell = tf.nn.rnn_cell.DropoutWrapper(cell, dtype=tf.float32, input_keep_prob=dropout, output_keep_prob=1.0,
                                                 state_keep_prob=1.0)
        cells.append(cell)
    if layers == 1:
        return cell
    else:
        return tf.contrib.rnn.MultiRNNCell(cells) #for sequential multiple cells.


class SimpleNlpRnn(object):
    def __init__(self, config):
        self.config = config

        with tf.name_scope("Embeddings"):
            self.embeddings = tf.get_variable("InputEmbeddings", [self.config.input_vocab_size, self.config.input_emb_size],
                                              dtype=tf.float32)

        with tf.name_scope("RNN"):
            with tf.variable_scope("rnn"):
                self.sequences_rnn_cell = cell(self.config.encoder_rnn_size, self.config.cell_type,
                                               dropout=self.config.encoder_keep_prob,
                                               config=config)

    def __call__(self, inputs, initial_state=None):
        self.sequences = inputs
        self.wes = tf.nn.embedding_lookup(self.embeddings,
                                          self.sequences)  # batch_size x sentence_max_len x word_emb_size

        outputs, state = tf.nn.dynamic_rnn(cell=self.sequences_rnn_cell,
                                           inputs=self.wes,
                                           initial_state=initial_state,
                                           dtype=tf.float32)

        if self.config.cell_type == "LSTM":
            state = state.h


        return state, outputs


