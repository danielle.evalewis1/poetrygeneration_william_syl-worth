# The LanguageModel class builds the language model using the SimpleNlpRnn cell. 
# The class builds the encoder, projection layer, cost function and applies the optimizer.
# This script is referenced in Chapter 3.3

# THis method does not use sampling, it uses greedy search in training and temperature sampling when generating from a trained model. 

#Import libraries  and TF1
import tensorflow as tf
import numpy as np

#Import SimpleNlpRnn class from submission code
from models.basics import SimpleNlpRnn


class LanguageModel(object):
    """
    language model used to learn the syllable-based poem generator.
    """
    def __init__(self, x, y, config, vocabulary, reuse=False, global_scope="LanguageModel"):
        """

        :param x: a tf.int32 tensor of shape batch_size x sentence_max_len.
        Input texts starts with <GO> symbol.
        :param y: a tf.int32 tensor of shape batch_size x sentence_max_len.
        y texts are the same as x WITHOUT <GO>. Therefore at position t
        y_t is the token to predict, while x_t is the token preceding y_t. For t=0
        y_t is preceded by <GO> symbol.
        :param config: instance of Config class.
        :param vocabulary: instance of Vocabulary class.
        :param reuse: True to share variables with same name, False otherwise.
        :param global_scope: name of the whole network.
        """

        with tf.variable_scope(global_scope, reuse=reuse):
            self.x = x  # batch_size x sentence_max_len
            self._y = y  # batch_size x sentence_max_len
            self.y = tf.reshape(self._y, [-1])
            self.vocabulary = vocabulary
            self.reuse = reuse
            self.global_scope = global_scope

            self.config = config

            with tf.variable_scope("Encoder", reuse=reuse):
                self.encoder = SimpleNlpRnn(config)  # encoder used in get_logits used to predict the next token

            with tf.name_scope("Output"):
                self.proj = tf.layers.Dense(self.config.proj_size)  # projection layer from encoder_state to the dimension of embeddings

                self.logits = self.get_logits(x)  # (batch_size x sentence_max_len) x vocab_size

                batch_size = tf.shape(self.x)[0]
                self.preds = tf.reshape(tf.argmax(self.logits, axis=1), [batch_size, -1])
                self.temp = tf.placeholder(dtype=tf.float32, shape=())
                self.sample = tf.reshape(tf.multinomial(self.logits / self.temp, num_samples=1), [batch_size, -1])

                self.p_w = tf.nn.softmax(tf.reshape(self.logits, shape=[-1, tf.shape(self.x)[1], config.input_vocab_size]))  # prob distribution over words in vocabulary

            with tf.variable_scope("Cost", reuse=reuse):
                self.tg_mask = tf.cast(tf.not_equal(self.y, tf.ones_like(self.y) * self.vocabulary.word2id("<PAD>")), tf.float32)
                ce = tf.nn.sparse_softmax_cross_entropy_with_logits(
                    labels=self.y, logits=self.logits)
                self.lm_loss = tf.reduce_sum(ce * self.tg_mask) / tf.reduce_sum(self.tg_mask)
                self.ppl = tf.exp(self.lm_loss)

                if not config.is_test:
                    with tf.name_scope("AdamOptiandmization"):
                        self.lr = config.learning.lr
                        if config.learning.lr_scheduler:
                            self.lr_ph = tf.placeholder(dtype=tf.float32, shape=())
                            optimizer = config.learning.optimizer(self.lr_ph)
                        else:
                            optimizer = config.learning.optimizer(self.lr)
                        gradients, variables = zip(*optimizer.compute_gradients(self.lm_loss))
                        gradients, _ = tf.clip_by_global_norm(gradients, config.learning.norm_clip)
                        self.train_op = optimizer.apply_gradients(zip(gradients, variables))

    def get_logits(self, x, initial_state=None):
        with tf.variable_scope("Encoder"):
            self.final_state, states = self.encoder(x, initial_state)  # get a vector for each token in the input
            self.states = tf.reshape(states, [-1, self.config.encoder_rnn_size])  # (batch_size x sentence_max_len) x encoder_rnn_size
        proj = self.proj(self.states)
        return tf.matmul(proj, tf.transpose(self.encoder.embeddings))  # (batch_size x sentence_max_len) x vocab_size

    def set_lr(self, lr):
        self.lr = lr


#%%

# Used in run_train_op and val_op
    def _get_std_feed_dict(self, b):
        """
        
        Creates feed_dict
        :param b: input batch sequences. 2-d tensor of size [batch_size, input_max_seq_len]
        and output batch sequences. 2-d tensor of size [batch_size, output_max_seq_len].
        :return: feed_dict
        """
        bx, by = b
        feed_dict = {self.x: bx, self._y: by}

        if not self.config.is_test and self.config.learning.lr_scheduler:
            feed_dict[self.lr_ph] = self.lr

        return feed_dict


    def run_train_op(self, sess, b, train_summary):
        """
        Evaluates the loss on x and y.
        :param sess: tf.Session() insance 
        :param b: batch tuple
        :param train_summary: tf.Summary for tracing training performances.
        :return: dictionary with values for training summaries, loss, perplexity values
        """
        feed_dict = self._get_std_feed_dict(b)
        run_output = sess.run((self.train_op, train_summary, self.lm_loss, self.ppl), feed_dict=feed_dict)

        run_output_dict = {
            "summaries": run_output[1],
            "loss": run_output[2],
            "ppl": run_output[3],
        }

        return run_output_dict

    def val_op(self, sess, v, val_summary):
        """
        Evaluates the loss on vx and vy.
        :param sess: tf.Session instance.
        :param v batch sequences. 2-d tensor of size [batch_size, output_max_seq_len].
        :param val_summary: tf.Summary for tracing validation performances.
        :return: value for validation summary, loss on the batch and perplexity ppl.
        
        """
        feed_dict = self._get_std_feed_dict(v)
        run_output = sess.run((val_summary, self.lm_loss, self.ppl), feed_dict=feed_dict)
        run_output_dict = {
            "summaries": run_output[0],
            "loss": run_output[1],
            "ppl": run_output[2],
        }
        run_output_dict["to_optimize_loss"] = run_output_dict["ppl"]

        return run_output_dict

    def gen_op(self, sess, gx):
        """
        Generates the output sequence, calculated with greedy search.
        :param sess: tf.Session instance.
        :param gx: input sequence. 2-d tensor of size [1, input_max_seq_len].
        :return: a sequence of size [1, output_max_seq_len] where the t-th element is the token y
        with highest P(y_t | x_0, ...., x_t-1).
        """

        feed_dict = self._get_std_feed_dict(gx)
        run_output = sess.run(self.preds, feed_dict=feed_dict)
        run_output_dict = {"preds": run_output}

        return run_output_dict

#%% These 2 functions are used for sampling generating text from a trained model.
#They are used in gen_op, gen_sess

    def batch_act(self, sess, x, temp=1.0):
        """
        Computes the next token (t+1 token) for a batch of sequences at time t.
        :param sess: tf.Session() instance
        :param x: 2-D tensor of shape [batch_size, time_t], i.e. a batch of sequences of length t.
        :param temp: temperature for multinomial sampling, set to 1.0 for no effects.
        :return: returns a 2-D numpy tensor of size [batch_size, 1]
        """

        return sess.run(self.sample, feed_dict={self.x: x, self.temp: temp})[:, -1:]

    def batch_generations(self, sess, n_episodes, temp):
        """
        Generates n_episodes of the same size. Use this method to create N verses.
        :param sess: tf.Session() instance
        :param n_episodes: an int. Number of verses to generate
        :param temp: temperature for multinomial sampling.
        :return: a 2-D numpy tensor of size [n_episodes, sentence_max_len]
        """

        input_eval = np.array([[self.vocabulary.word2id("<GO>")]] * n_episodes)  # initialize all the sequences with the starting token
        for i in range(self.config.sentence_max_len):
            o = self.batch_act(sess, input_eval, temp=temp)  # batch_size x 1
            input_eval = np.concatenate((input_eval, o), axis=1)

        return input_eval[:, 1:]

