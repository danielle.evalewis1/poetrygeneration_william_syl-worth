# This script contains functions for the following:
# 1. General Utility functions,
# 2. Dictionary and padding
# 3. Tokenization and cleaning
# 4. Hyphenation - Hyphenate 
# 5. Hyphenation - Pyphen (not used)
   

# Import Libraries 

import string
import collections
import os
import re
import pickle

#library for Tokenizing
from nltk.tokenize import TweetTokenizer # softer tokenizer than word_tokenize

#library for hyphenating
from hyphenate import hyphenate_word


#%% 1. General Utility functions 

# saves data as pickle files 
def save_data(data, file):
    with open(file, 'wb') as output: #'wb' for reading binary files - best practice
        pickle.dump(data, output, pickle.HIGHEST_PROTOCOL)

# loads pickle files 
def load_data(file):
    with open(file, 'rb') as obj: # 'rb' for reading binary files - best practice 
        return pickle.load(obj)

# writes x to file 
def print_and_write(file, x):
    print(x)
    file.write(x)


# Converts Ground Truth and Prediction IDs to strings and then writes strings to file 
def print_paired_output(file, batch_y, batch_z, rev_vocabulary, special_tokens, end_of_tokens=None):
    '''
    Converts output from a batch of IDs into strings and writes to file
    :param file: file name(string) to save
    :param batcy_y: Grount Truth
    :param batch_z: Prediction
    :reverse_dictionary: e.g. {ID: "Word"}
    :param special_tokens: Optional. Useful to add special tokens in vocabulary. 
    : end_of_tokens = None


     '''
    def output2string(batch, rev_vocabulary, special_tokens, end_of_tokens):
        output_strings = []
        for seq in batch:
            to_print = ''
            for token in seq:
                if token in special_tokens:
                    to_print += ' ' # prints a space if output token is in special tokens 
                elif end_of_tokens and token in end_of_tokens:
                    to_print += '\n' #next line if end_of_tokens.
                elif token in rev_vocabulary:
                    to_print += rev_vocabulary[token] # prints token in vocabulary
                else:
                    to_print += '<UNK>' # if token not in vocabulary, prints 'unk'
            output_strings.append(to_print)

        return output_strings

    hyps_y = output2string(batch_y, rev_vocabulary, special_tokens, end_of_tokens) #converts ground truth ID  to string 
    hyps_z = output2string(batch_z, rev_vocabulary, special_tokens, end_of_tokens) #converts predicted ID to string

    # Writes the string output to file for ground truth and prediction
    for i in range(len(hyps_y)):
        print_and_write(file, "\n================================================")
        print_and_write(file, 'Ground Truth: ' + hyps_y[i] + "\n")
        print_and_write(file, 'Prediction: ' + hyps_z[i] + "\n")
        print_and_write(file, "================================================\n")



#%% 2. Dictionary and padding

# Only used for testing, not used in final experiment
def build_dataset_of_tokens(tokens, vocabulary_size, special_tokens=[]):
    '''
    Given a list of tokens, it creates a dictionary mapping each token to a unique id.
    :param tokens: a list of token strings. (all the tokens of the corpus)
    :param vocabulary_size: The number of elements of your vocabulary. If there are more
    than 'vocabulary_size' elements in tokens param, it takes only the 'vocabulary_size'
    most frequent ones.
    :param special_tokens: Optional. Useful to add special tokens in vocabulary. I
    f you don't have any, keep it empty.
    :return: 
     data: the mapped tokens list;
     count: a dictionary containing the count of occurrences for each
     element on your dictionary.
     dictionary: a python dictionary that associates a token with a unique integer ID.e.g. {"word":1}
     reverse_dictionary: a python dictionary mapping a unique integer ID to its token.{1:"word"}
     
    '''
    # counting occurrences of each token
    count = [['UNK', -1]]
    count.extend(collections.Counter(tokens).most_common(vocabulary_size - 1))  # takes only the most frequent ones of 'vocabulary_size'
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary) #counts occurences

    for token in special_tokens:
        dictionary[token[0]] = token[1] #counts special tokens occurences

    data = list()
    unk_count = 0
    for word in tokens:
        if word in dictionary:
            index = dictionary[word] #assigns integer ID
        else:
            index = 0  # dictionary['UNK']
            unk_count += 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys())) #reverses the dictionary
    return data, count, dictionary, reverse_dictionary




def pad_list(l, pad_token, max_l_size, keep_lasts=False, pad_right=True):
    """
    Adds a padding token to a list
    :param l: input list to pad.
    :param pad_token: token to add as padding.
    :param max_l_size: maximum length of the new padded list to return
    and truncates lists longer than 'max_l_size'.
    :param keep_lasts: If True, preserves the max_l_size tail elements
    of a sequence 
    :param pad_right: If True, pads to right. If False, pads to left.


    :return: the list padded or truncated.
    """
    to_pad = []
    max_l = min(max_l_size, len(l))  # maximum len of list to pad
    l_init = len(l) - max_l if len(l) > max_l and keep_lasts else 0  # initial position where to sample from the list
    l_end = len(l) if len(l) > max_l and keep_lasts else max_l
    for i in range(l_init, l_end):
        to_pad.append(l[i])

    pad_tokens = [pad_token] * (max_l_size-len(l)) # adds tokens to the len of l and max_l
    padded_l = to_pad + pad_tokens if pad_right else pad_tokens + to_pad # pad to right else pad to left

    return padded_l



#%% 3. Tokenization and cleaning
# Mentioned in 3.1.2 Data Parsing

tknzr = TweetTokenizer() # instantiates tokenizer

# Used for Wordsworth Production data and Prelude data
# Reads file, cleans and tokenizes text
def get_1_verse_production(filename, encoding =None):
    """
    Reads filename/s and strips punctuation 
    :param filename: data source
    :param encoding: 'utf-8' for Prelude, 'None' for Production
    

    :return: 
    :words: list of tokenized words
    :verses: list of verses (1 line)
    
    """
    N=1 #change this to alter the verse quantity in each list e.g. N= 2 =2 verses
    verses, words = [], []
    filelist = os.listdir(filename)

    for i in filelist:
        if i.endswith(".txt"):  # reads all .txt files from source 
            with open(filename + i, 'r', encoding=encoding) as f:

                verses_no_empty_lines = [x for x in f if x.strip()] #removes empty lines
                [re.split("-|â€”|€|â", w) for w in verses_no_empty_lines] #removes encoding problems  
                for v, verse in enumerate(verses_no_empty_lines): #iterates through the list
                    if v % N == 0:
                        verses.append([])
                    # lowercase and tokenizes with nlth tknzr.tokenize
                    tokenized_sentence = [w.lower().strip() for w in tknzr.tokenize(verse)  if len(w) > 0] 
                    
                    #list of additonal punctuation removed
                    tokenized_sentence = [w for w in tokenized_sentence if "," not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "." not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "?" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if ":" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "-" not in w]              
                    tokenized_sentence = [w for w in tokenized_sentence if ";" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "«" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "»" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "'" not in w] # removes apostrophes e.g. mak'st -> makst
                    tokenized_sentence = [w for w in tokenized_sentence if "‘" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "‘‘" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "(" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if ")" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "!" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "*" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '"' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '”' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '€' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if 'â' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '¨' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '_' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if ']' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '[' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '—' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '/' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '“' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '’' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '&' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '™' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '•' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '>' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '¦' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '£' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '©' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '^' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "\\" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '``' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '†' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '$' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '%' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if not w.isdigit()] #removes all integer numbers (chapter/line numbers)

                    ts = []
                    [ts.extend(re.split("(\')", e)) for e in tokenized_sentence]
                    tokenized_sentence = [w for w in ts if len(w) > 0]
                    verses[-1].append(tokenized_sentence)
                    words.extend(tokenized_sentence)

    return verses, words


# Used for guide to the lakes - 1 line 
# Reads file, cleans and tokenizes text
def get_guide_to_the_lakes(filename, encoding =None):
    """
    Reads filename/s and strips punctuation 
    :param filename: data source
    :param encoding: 'utf-8' for Prelude, 'None' for Production
    

    :return: 
    :words: list of tokenized words
    :verses: list of verses (1 line)
    
    """
    
    verses, words = [], []
    with open(filename, "r", encoding=encoding) as f: #reads from source # encoding = 'utf-8 for Guide
        for line in f:
            raw_sentences = line.strip() #removes empty lines
            if len(raw_sentences) > 1:
                sentences = raw_sentences.translate(string.punctuation)
                s_list = re.split("\.", sentences) #split into lines based on full stop 
                [re.split("-|â€”|€|â", w) for w in s_list] #removes encoding problems  
                
                for s in s_list: #iterates through the list
                    
                    #list of additonal punctuation removed, #comment to keep
                    tokenized_sentence = tknzr.tokenize(s)# tokenizes with nlth tknzr.tokenize
                    tokenized_sentence = [w.lower() for w in tokenized_sentence if len(w)>0] #lowercases
                    tokenized_sentence = [w for w in tokenized_sentence if "," not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "." not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "?" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if ":" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "-" not in w]              
                    tokenized_sentence = [w for w in tokenized_sentence if ";" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "«" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "»" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "'" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "‘" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "‘‘" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "(" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if ")" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "!" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "*" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '"' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '”' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '€' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if 'â' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '¨' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '_' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if ']' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '[' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '—' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '/' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '“' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '’' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '&' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '™' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '•' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '>' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '¦' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '£' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '©' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '^' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if "\\" not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '``' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '†' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '$' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if '%' not in w]
                    tokenized_sentence = [w for w in tokenized_sentence if not w.isdigit()] #removes integers
    
                    ts = []
                    [ts.extend(re.split("(\')", e)) for e in tokenized_sentence]
                    tokenized_sentences = [w for w in ts if len(w) > 0]
    
                    if len(tokenized_sentences) > 1: 
                        verses.append(tokenized_sentences)
                        words.extend(tokenized_sentences)

    return verses, words


#%% 4. Hyphenation - Hyphenate 

# This was succesfully used to hyphenate the tokens into syllables

# Library for Hyphenating 
#from hyphenate import hyphenate_word
 
#implements hyphenate to individual token
def short_hyphenate(word):
    h1= hyphenate_word(word) #implements hyphenate to word
    t1= [ t for t in h1 if len(t)>0 ] #remove empty tokens from vocab 
    return t1 


# used for production and the prelude 
#implements hyphenate to a list of tokens
def get_hyp_lm_verses_works(verses): 
    """
    Hyphenates a list of verses 
    :param verses: list of strings

    :return: 
    :new_verses: list of hyphenated verses with SEP tokens between words 
    
    """
    
    new_verses = []
    for verse in verses:
        new_verses.append([])  
        for line in verse:
            new_verses[-1].append([])
            for hyp_w in line:
                processed_word=short_hyphenate(hyp_w) #hyphenates using hyphenate_word library

                new_verses[-1][-1].extend(processed_word)
 
                new_verses[-1][-1].append('<SEP>')# SEP tokens are added inbetween words
   
            new_verses[-1][-1] = new_verses[-1][-1][:-1]

    return new_verses

#used for the Guide 
#implements hyphenate to a list of tokens
def get_hyp_lm_verses_guide(verses): 
    """
    Hyphenates a list of verses 
    :param verses: list of strings

    :return: 
    :new_verses: list of hyphenated verses with SEP tokens between words 
    
    """
    new_verses = []
    for line in verses:
        new_verses.append([])
        for hyp_w in line:
            processed_word=short_hyphenate(hyp_w) #hyphenates using hyphenate_word library

            new_verses[-1].extend(processed_word)

            new_verses[-1].append('<SEP>') # SEP tokens are added inbetween words
   
        new_verses[-1] = new_verses[-1][:-1]

    return new_verses




#%%  5. Hyphenation - Pyphen (not used)

# This was the Pyphen implementation which is not used in the experiments 

import pyphen
#dic = pyphen.Pyphen(lang='en_GB')

# performs pyphens hyphenation on individual word
def short_hyph(word):
    dic = pyphen.Pyphen(lang='en_GB') #import language dictionary
    h1=dic.inserted(word) #hyphenates and returns hyphenated words e.g. 'hyph-en' 
    t1 = [x for x in h1.split('-') ] #splits the word on the '-' e.g. 'hyph', 'en'
    t1= [ t for t in t1 if len(t)>0 ] #remove empty strings from vocab 
    return t1

# Pyphen hyphenation implementation on a list of verses 
def hyphenate(verses):
    """
    Hyphenates a list of verses 
    :param verses: list of strings

    :return: 
    :new_verses: list of hyphenated verses with SEP tokens between words 
    :hyp_tokens: list of hyphenated tokens 
    """
    dic = pyphen.Pyphen(lang='en_GB')
    hyp_verses, hyp_tokens = [], []
    for list_ in verses:
        for lis in list_:
            hyphenate= []
            for word in lis:   
                t1= dic.inserted(word) #hyphenates and returns hyphenated words e.g. 'hyph-en' 
                t1 = [x for x in t1.split('-') ] #splits the word on the '-' e.g. 'hyph', 'en'
                hyphenate.append(t1)
                hyp_tokens.extend(t1)
            t2=[ y for x in hyphenate for y in x]
            hyp_verses.append(t2)
           
    return hyp_verses, hyp_tokens 


# The following 2 functions were used to try and improve the Pyphen implementation 
# using the rules states in  4.1 Hyphenation in the report 

# splits words based on the last 2 characters
def improved(i):
    i= list(i.partition(i[-2:])) 
    i= i[0:2]
    return i

# 'improving ' pyphen module
def get_hyp_lm_verses(verses): 
    """
    Improves the results of Pyphen hyphenation by hyphenating false negative words 
    based on end 2 characters and pattern
    
    :param verses: list of strings

    :return: 
    :new_verses: list of hyphenated verses  
    
    """
    new_verses = []
    for verse in verses:
        new_verses.append([])  
        for line in verse:
            new_verses[-1].append([])
            for hyp_w in line:
                processed_word=short_hyph(hyp_w) #hyphenates using Pyphen library 
                if len(processed_word)>0:
                    # if the last 2 characters of each word match the patterns listed 
                    if (len(processed_word[-1]) >2) and ((processed_word[-1][-2:]=='ty' ) 
                                                         or(processed_word[-1][-2:]=='ly') 
                                                         or (processed_word[-1][-2:]=='er') 
                                                         or (processed_word[-1][-2:]=='le' )
                                                         or (processed_word[-1][-2:]=='dy' ) 
                                                         or (processed_word[-1][-2:]=='en' )): #catch all improvement to hyphenation 
  
                        new_hyp=improved(processed_word[-1])
                        processed_word=processed_word[:-1]
                        processed_word.extend(new_hyp)
                    new_verses[-1][-1].extend(processed_word)

                    new_verses[-1][-1].append('<SEP>')# adds SEP tokens between words
   
            new_verses[-1][-1] = new_verses[-1][-1][:-1]

    return new_verses

