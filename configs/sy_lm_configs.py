# This script creates the LearningConfig class and SyLMConfig class 


# Import libraries and TF1
import copy
import tensorflow as tf


class LearningConfig:
    def __init__(self, optimizer=tf.compat.v1.train.AdamOptimizer, lr=0.001, norm_clip=100.0, batch_size=32,
                 lr_scheduler=True, max_no_improve=5, n_epochs=10,
                 trunc_norm_init_std=1e-4, is_test=False):
        self.batch_size = batch_size
        self.n_steps = 3500
        self.n_epochs = n_epochs
        self.lr = lr

        self.optimizer = optimizer
        self.norm_clip = norm_clip

        self.lr_scheduler = lr_scheduler
        self.max_no_improve = max_no_improve

        self.trunc_norm_init_std = trunc_norm_init_std

        self.is_test = is_test


class SyLMConfig:
    def __init__(self, vocab_size,
                 sentence_max_len, emb_size,
                 rnn_size, cell_type, keep_prob,
                 proj_size,
                 optimizer, lr, norm_clip, batch_size, n_epochs, lr_scheduler, max_no_improve,
                 restore_model,
                 is_test=False):
        """
        Language Model Configuration Class
        :param vocab_size:
        :param is_test:
        """

        self.is_test = is_test
        # General
        self.input_vocab_size = self.output_vocab_size = vocab_size
        self.sentence_max_len = sentence_max_len
        self.input_emb_size = self.output_emb_size = emb_size

        # Encoder
        self.encoder_rnn_size = rnn_size
        self.encoder_keep_prob = keep_prob if not is_test else 1
        self.cell_type = cell_type
        self.wrap_attention = False

        self.proj_size = proj_size

        # Learning
        self.learning = LearningConfig(optimizer, lr, norm_clip, batch_size, lr_scheduler, max_no_improve, n_epochs=n_epochs, is_test=is_test)
        self.restore_model = restore_model



def setup_config(FLAGS):
    """
    :param FLAGS:
    :return:
    """

    config = SyLMConfig(vocab_size=FLAGS.vocab_size, sentence_max_len=FLAGS.sentence_max_len, emb_size=FLAGS.emb_size,
               rnn_size=FLAGS.enc_size, cell_type=FLAGS.rnn_cell_type, keep_prob=FLAGS.enc_keep_prob, proj_size=FLAGS.proj_size,
               optimizer=tf.train.AdamOptimizer, lr=FLAGS.lr, norm_clip=FLAGS.norm_clip, batch_size=FLAGS.batch_size, n_epochs=FLAGS.n_epochs,
               lr_scheduler=FLAGS.lr_scheduler, max_no_improve=FLAGS.max_no_improve, restore_model=FLAGS.restore_model, is_test=False)

    val_config = copy.deepcopy(config)
    val_config.encoder_keep_prob = 1.0
    val_config.is_test = True

    gen_config = copy.deepcopy(val_config)

    return config, val_config, gen_config
