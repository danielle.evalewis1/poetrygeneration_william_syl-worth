"""
This EXPERIMENT script can be used to perform training using the syllable language model 
and to generate poetry from a trained model

Greedy search implementation
Temperature sampling generation

"""


#Import Libraries 
import tensorflow as tf
from shutil import copyfile

from datetime import datetime
import pickle 

# Import classes and functions from the submission code folders 
from datasets.word_syllable_dataset import wordsworthSyLMDatasetProduction, wordsworthSyLMDatasetGuide
from configs.sy_lm_configs import setup_config
from models.language_model import LanguageModel
from experiments.syllable_lm_experiment import SyLMExperiment
from utils.utils import save_data, print_and_write, load_data


# Change the working directory to where you have the saved the submission code here
import os
os.chdir(r'C:\Users\danie\Desktop\Dissertation\The_Code\nlgpoetry-v1.0.2')
#%% Declare FLags 

"""
Here you can change the values for different parameters to train a model
If you are training a model on pre-trained weights (transfer learning) you must
use the same network configurations. However the flags under #learning can be fine-tuned.

Change experiment mode to 'eval' to generate from a trained model 

"""

# All Experiment parameters
FLAGS = tf.app.flags.FLAGS

# Experiment Mode: train | eval
tf.app.flags.DEFINE_string('exp_mode', "train", 'Train or Evaluation') # train will conduct training, eval will generate from a model

# Vocabulary and Embeddings
tf.app.flags.DEFINE_integer('vocab_size', 8000, 'size of the vocabulary') #the size of the vocabulary used
tf.app.flags.DEFINE_integer('emb_size', 250, 'size of word embeddings') 

# RNNs
tf.app.flags.DEFINE_string('rnn_cell_type', "LSTM", 'cell type') # Could change this to GRU

# Encoder
tf.app.flags.DEFINE_integer('sentence_max_len', 40 , 'sequence max length ') 
tf.app.flags.DEFINE_integer('enc_size', 768, 'size of encoder state (if bidirectional it is doubled) ') 
tf.app.flags.DEFINE_float('enc_keep_prob', 0.3, 'encoder dropout probability') 
tf.app.flags.DEFINE_integer('proj_size', 250, 'size of projection layer')

# Learning

# Change this for transfer learning to True. False if training a model without pre-training.
tf.app.flags.DEFINE_bool('restore_model', True, 'restore weights from a pretrained model') 

tf.app.flags.DEFINE_integer('n_epochs', 25, 'max number of epochs') 
tf.app.flags.DEFINE_integer('batch_size', 32 , 'batch size') 
tf.app.flags.DEFINE_float('lr', 0.002, 'learning rate') 
tf.app.flags.DEFINE_float('norm_clip', 4.0, 'gradient clip norm') 
tf.app.flags.DEFINE_integer('max_no_improve', 20, 'number of validations tests without improvements')
tf.app.flags.DEFINE_bool('lr_scheduler', True, 'decreases by half the lr when the model does not improves and it is restored') #true
tf.app.flags.DEFINE_bool('batch_scheduler', False, 'decreases by half the lr when the model does not improves and it is restored')

#%% Run the experiment!


starttime= datetime.now() # starts the clock 

if __name__ == '__main__':
    is_test = False
    base_dir = os.path.join(os.getcwd(), "Underfit")  # experiments directory
    exp_dir = "Prelude_Prod_Guide_with_768" # name of the folder to save configs and logs 
    exp_path = os.path.join(base_dir, exp_dir)

    if not os.path.exists(exp_path):
        os.mkdir(exp_path)  # this is the experiment directory
        # copies files for reproducibility purposes
        copyfile(os.path.join(os.getcwd(), os.path.basename(__file__)), os.path.join(exp_path, os.path.basename(__file__)))

    # Getting Configurations
    config, val_config, gen_config = setup_config(FLAGS)

    # Creating Dataset and Vocabulary
    '''To create a dataset with a given vocabulary set vocab.  
       Set vocab = None to create a vocabulary with the dataset'''
    
    # Load the vocab you want to use    
    vocab =  load_data(os.path.join(exp_path, 'vocabulary_production_and_prelude_and_guide_combined_V2.pkl')) #hash this out if using no vocab
    poetry_sy_lm_dataset = wordsworthSyLMDatasetProduction(config, vocab) # create vocabulary object with vocab (vocab = None if not used)
    #poetry_sy_lm_dataset = wordsworthSyLMDatasetGuide(config, vocab)  #use this for the Guide 
    
    # Get the data_path for the datsets you want to train with 
    #data_path = os.path.join(os.getcwd(), "data\\") 
    data_path = os.path.join(os.getcwd(), "data\\prelude\\") 
    #data_path = os.path.join(os.getcwd(), "data\\guide\\guide_to_the_lakes.txt")
     
    # actual creation of  vocabulary (if not provided) and dataset for training, validation and test set                  
    poetry_sy_lm_dataset.build(data_path, split_size=0.9)  # change the size of training/test sets
    print("Train size: " + str(len(poetry_sy_lm_dataset.train_y)))
    print("Val size: " + str(len(poetry_sy_lm_dataset.val_y)))
    print("Test size: " + str(len(poetry_sy_lm_dataset.test_y)))

    # Saving Configs
    save_data(config, os.path.join(exp_path, 'configs.pkl'))
    save_data(val_config, os.path.join(exp_path, 'val_configs.pkl'))
    save_data(gen_config, os.path.join(exp_path, 'gen_configs.pkl'))

    # Saving Vocabulary for future 
    poetry_sy_lm_dataset.vocabulary.save_vocabulary(os.path.join(exp_path, 'vocabulary.pkl'))
    

    # Building Model
    '''Three different model instances, one for training, one for validation and the last for testing.
    Training model has dropout, validation and test models don't. In general there may be other
    different configurations specified for each model. They all share the same weights (reuse=True allows that).'''
    x_ph = tf.placeholder(dtype=tf.int32, shape=[None, None])  # batch_size x sentence_max_len
    y_ph = tf.placeholder(dtype=tf.int32, shape=[None, None])  # batch_size x sentence_max_len
    model_name = "WordSyllableLMPoemGenerator_Production"
    sy_lm_net = LanguageModel(x_ph, y_ph, config, poetry_sy_lm_dataset.vocabulary ,  global_scope=model_name) #poetry_sy_lm_dataset.vocabulary
    sy_lm_net_val = LanguageModel(x_ph, y_ph, val_config, poetry_sy_lm_dataset.vocabulary , global_scope=model_name, reuse=True) #poetry_sy_lm_dataset.vocabulary
    sy_lm_net_gen = LanguageModel(x_ph, y_ph, gen_config, poetry_sy_lm_dataset.vocabulary , global_scope=model_name, reuse=True) #poetry_sy_lm_dataset.vocabulary

    # Experiment Run
    '''SyLMExperiment object handles all the experiment. It requires 3 model instances, the dataset and the configuration object.
    It logs performances, tensorflow summaries, and outputs during the training. WHen FLAGS.lr_scheduler is set to True, it 
    also handles model checkpointing and early stopping.'''
    lm_exp = SyLMExperiment(sy_lm_net, sy_lm_net_val, sy_lm_net_gen, poetry_sy_lm_dataset, exp_path, config)



    mode = FLAGS.exp_mode
    if mode == "train":
        print("\nTraining\n")
        lm_exp.config.restore_path = exp_path # unhash IF you are restoring weights from a trained model
        lm_exp.run() # This runs training and validation 
        #lm_exp.config.restore_path = exp_path # Unhash if you are training a model without restoring weights
        test_ppl = lm_exp.test(poetry_sy_lm_dataset)
        print("PPL on Testset {}".format(test_ppl)) # Prints the perplexity of the trained model on the test set
    else:
        print("\nGeneration\n")
        # Restores weights and generates from the trained models weights.
        lm_exp.config.restore_path = exp_path
        generated = lm_exp.run_gen_sess(100, 1.0)  #change the number of generations and temperature

print(datetime.now()- starttime) # Stops the clock so we know how long it took.


#%%
# Saving the results! 
ids= generated[0]
words= generated[1]

with open('generated_text_model_prelude_0.0001 0.5 .pkl', 'wb') as f:
   pickle.dump(words, f)
  
with open('generated_ID_model_ prelude 0.0001 0.5 .pkl', 'wb') as f:
   pickle.dump(ids, f)

